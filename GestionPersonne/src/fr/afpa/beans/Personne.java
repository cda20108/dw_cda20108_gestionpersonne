package fr.afpa.beans;

import java.io.Serializable;
import java.time.LocalDate;

public class Personne implements Serializable{

	
	private String nom;
	private String prenom;
	private LocalDate dateNaiss;
	
	
	
	public Personne(String nom, String prenom, LocalDate dateNaiss) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
	}
	public Personne() {

	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public LocalDate getDateNaiss() {
		return dateNaiss;
	}
	public void setDateNaiss(LocalDate dateNaiss) {
		this.dateNaiss = dateNaiss;
	}
	
	@Override
	public String toString() {
		return nom + ";" + prenom + ";" + dateNaiss;
	}
	
	
}
