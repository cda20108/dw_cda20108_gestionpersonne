package fr.afpa.app;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import fr.afpa.beans.Personne;

public class Main {

	private final static String FILE_NAME = "C:\\ENV\\personne.data";

	public static void main(String[] args) throws IOException {
		String choix = "*";
		Scanner scan = new Scanner(System.in);
		while (!(choix.equalsIgnoreCase("Q"))) {
			menu();
			System.out.println("Choisissez une option : ");
			choix = scan.next();
			switch (choix) {
			case "A":
			case "a":
				ajouterPersonne();
				break;
			case "B":
			case "b":
				afficherList();
				break;
			case "C":
			case "c":
				int indicePersonne = rechercherPersonne();
				if (indicePersonne == -1) {
					System.out.println("Personne non trouv�");
				} else {
					System.out.println("Voici les informations de cette personne : " + lirePersonne(indicePersonne));
				}
				break;
			case "D":
			case "d":
				supprimerPersonne();
				break;
			case "E":
			case "e":
				modifierPersonne();
				break;
			case "Q":
			case "q":
				System.out.println("Au revoir");
				break;
			default:
				System.out.println("Votre choix ne correspond � aucune Commande, veuillez recommencer");
				break;
			}
		}

	}

	private static void menu() {

		System.out.println("-----------------------------MENU GESTION PERSONNE -----------------------------\n"
				+ "*                                                                                * \n"
				+ "*                   A- Ajouter une personne                                      * \n"
				+ "*                   B- Liste des personnes                                       * \n"
				+ "*                   C- Rechercher une personne                                   * \n"
				+ "*                   D- Supprimer une personne                                    * \n"
				+ "*                   E- Modifier une personne                                     * \n"
				+ "*                   Q- Quitter le menu                                           * \n"
				+ "-----------------------------------------------------------------------------------\n");

	}

	private static void afficherList() {
		ArrayList<Personne> listPersonne = lecture(FILE_NAME);
		if (listPersonne != null) {
			Scanner scan = new Scanner(System.in);
			System.out.println("Choisissez une limite d'affichage de personne : ");
			int limite = scan.nextInt();
			for (int i = 0; i < listPersonne.size() && i < limite; i++) {
				System.out.println("-------------------------------------------------");
				System.out.println(listPersonne.get(i));
				System.out.println("-------------------------------------------------");
			}
		}

	}

	private static Personne lirePersonne(int indicePersonne) {
		ArrayList<Personne> listPersonne = lecture(FILE_NAME);
		return listPersonne.get(indicePersonne);
	}

	private static void modifierPersonne() {
		Scanner scan = new Scanner(System.in);
		ArrayList<Personne> listPersonne = lecture(FILE_NAME);
		int indicePersonne = rechercherPersonne();
		if (indicePersonne != -1) {
			System.out.println("Donnez moi votre prenom");
			listPersonne.get(indicePersonne).setPrenom(scan.next());
			System.out.println("Donnez moi votre nom");
			listPersonne.get(indicePersonne).setNom(scan.next());
			System.out.println("Donnez moi votre date de naissance (format : jj/mm/aaaa)");
			listPersonne.get(indicePersonne)
					.setDateNaiss(LocalDate.parse(scan.next(), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			reecrireFichier(FILE_NAME, listPersonne);
		}
	}

	public static ArrayList<Personne> lecture(String s) {
		ArrayList<Personne> people = new ArrayList<Personne>();
		try {
			FileInputStream f = new FileInputStream(new File(s));
			ObjectInputStream ois = new ObjectInputStream(f);
			people = (ArrayList<Personne>) ois.readObject();
			ois.close();

		} catch (FileNotFoundException e) {
			System.out.println("fichier introuvable");
		} catch (EOFException e) {
			System.out.println("fin de lecture");
		} catch (Exception e) {
			System.out.println("Erreur " + e);
		}
		return people;
	}

	public static void ajouterPersonne() {
		Scanner scan = new Scanner(System.in);
		Personne pers = new Personne();
		ArrayList<Personne> listPersonne;
		try {
			listPersonne = lecture(FILE_NAME);
			System.out.println("Donnez moi votre prenom");
			pers.setPrenom(scan.next());
			System.out.println("Donnez moi votre nom");
			pers.setNom(scan.next());
			System.out.println("Donnez moi votre date de naissance (format : jj/mm/aaaa)");
			pers.setDateNaiss(LocalDate.parse(scan.next(), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			listPersonne.add(pers);
			reecrireFichier(FILE_NAME, listPersonne);
		} catch (NullPointerException e) {
			listPersonne = new ArrayList<Personne>();
			System.out.println("NPE Exception");
		}

	}

	public static int rechercherPersonne() {
		Scanner scan = new Scanner(System.in);
		ArrayList<Personne> listPersonne = lecture(FILE_NAME);
		System.out.println("Donnez moi le nom de la personne � rechercher");
		String nom = scan.next();
		int i = -1;
		for (Personne personne : listPersonne) {
			i++;
			if ((personne.getNom()).equalsIgnoreCase(nom)) {
				System.out.println("Est-ce la personne que vous cherchez ? : "+ lirePersonne(i));
				System.out.println("O/N");
				String choix = scan.next();
				if("O".equalsIgnoreCase(choix)) {
					return i;
				}
			}

		}
		return -1;
	}

	public static void supprimerPersonne() {
		int indicePersonne = rechercherPersonne();
		ArrayList<Personne> listPersonne = lecture(FILE_NAME);
		if (indicePersonne != -1) {
			listPersonne.remove(indicePersonne);
			reecrireFichier(FILE_NAME, listPersonne);
		}
	}

	public static void reecrireFichier(String s, ArrayList<Personne> lp) {
		FileOutputStream f = null;
		ObjectOutputStream oos = null;
		try {
			File fichier = new File(s);
			f = new FileOutputStream(fichier);
			oos = new ObjectOutputStream(f);
			oos.writeObject(lp);
			oos.writeInt(lp.size());
			oos.close();

		} catch (Exception e) {
			System.out.println("Erreur" + e);
		}
	}

}
